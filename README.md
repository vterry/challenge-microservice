# Microservice Challenge: Interaction Service


## 1. Introdução

Este projeto tem por objetivo fornecer uma solução JAVA para o Desafio Microsserviços proposto pela Kroton em parceira com a Sysmap.

#### 1.1. Cenário:

Uma empresa de telecomunicação tem por objetivo modernizar seu sistema de gerenciamento de clientes. Dentre as funcionalidades que este possui, foi priorizado o desenvolvimento da funcionalidade de Registro de Interações do cliente com a empresa em novo cenário *Greenfield*. 

#### 1.2. MVP:

Foi definido pelo cliente que uma primeira entrega de valor agregado deverá ser capaz de:

**Fornecer protocolo:**  Gera um código único, mas legível pelo cliente, utilizado como identificador de um atendimento. Todas as solicitações, reclamações, elogios, etc., realizadas pelo cliente, serão marcadas com esse código.

**Registrar manifestações do cliente:** Registro simples de manifestações do cliente, tais como: elogíos, reclamações e sugestões. Esses registros serão utilizados por algumas áreas e sistemas internos de atendimento da empresa, por exemplo: ouvidoria, atenção ao cliente, entre outras.

### 2. Arquitetura Proposta:

Com o intuito de simplificar o desenvolvimento e manutenção das novas funcionalidades, foi considerado a adoção de um Padrão de Arquitetura Orientada a Evento conhecido por **Event Sourcing**. Dessa forma, todo registro de interação entre Cliente e Empresa será tratado como um evento de atendimento disponível para todas as aplicações, capaz de representar o estado atual de um registro com base manifestações do cliente.

#### 2.1. Arquitetura de  Referência

![](https://i.imgur.com/PN6xTpe.png)

Toda a solução esta disponivel dentro de um Container Docker. Esta consiste em um API Gateway responsável por fazer a segurança e roteamento para os microserviços. A aplicação Interaction possui um banco de dados Relacional. Todos os endpoints estão registrados no JHipster Registry.

##### 2.1.1 Ferramentas adotadas

- **JHipster:** Utilizado como gerador de código para a base da aplicação. Foi selecionado esta ferramenta devido sua facilidade de instalação e configuração. O JHipster hoje disponiliza um vasto conjunto de ferramentas voltadas para a arquitetura de microsserviços, tais como EurekaServer para registry de endpoints, Traefik para balanceamento de carga e proxy reverso, além de integração nativa com ELK. (http://www.jhipster.tech/microservices-architecture/)

- **PostgresSQL**: Banco de dados relacional OpenSource muito utilizado no mundo corporativo em alternativa a outras soluções Enterprise. Foi selecionado este banco devido sua robustez, dado que estamos trabalhando dentro de uma arquitetura que exige performance e alto volume de chamadas no banco e dados. O Postgres será nosso *Event Sourcing Engine* que armazenará todos os eventos registrados pela aplicação.
#### 2.2. Modelo de Dados

![](https://i.imgur.com/f6Ihtfw.png)

Será considerado que todo procolo representa um Evento de Interação do usuário com a Empresa. Esse evento será armazenado em uma única tabela. Nesta tabela temos informações de rastreabilidade  do Evento, tais como o numero do protocolo (protocol_number), tipo do evento, feedbacks relacionados ao eventos caso existam, e a data de atualização do evento. Como estamos trabalhando dentro do Pattern *Database Per Microservice*, temos algumas outras informações que podem ajudar as aplicações clientes a relacionar o evento a um Usuário, bem como a Aplicações. 

### 3. Configurando o Ambiente

#### 3.1. Requisitos

Antes de iniciar a instalação, certifique-se que você possui em seu ambiente:

- Java superior a versão 6
- Docker
- NodeJS

O projeto esta disponivel no GIT: (https://gitlab.com/vterry/challenge-microservice.git).

```unix
git clone https://gitlab.com/vterry/challenge-microservice.git
```

#### 3.2. Instalando o JHipster

```unix
1. npm install -g npm
2. npm install -g yarn
3. npm install -g yo
4. npm install -g generator-jhipster
```

#### 3.3. Build do Projeto

Dentro do projeto temos 3 subpastas:

- Interaction: diretório da nossa aplicação.
- Gateway: diretório do gateway.
- Docker: diretório com as configurações do nosso Container.

**Iniciando o build:**

For Unix/OSX
```unix
cd interaction/
./gradlew -Pprod bootRepackage buildDocker

cd ../gateway/
./gradlew -Pprod bootRepackage buildDocker
```

For Windows
```unix
cd interaction/
./gradlew.bat -Pprod bootRepackage buildDocker

cd ../gateway/
./gradlew.bat -Pprod bootRepackage buildDocker
```
#### 3.4. Iniciar o Docker Compose

```unix
docker-compose up
```

### 4. Como utilizar a Aplicação

A aplicação disponibliza 4 endpoints, dois que representam ações para gerar ou adicionar novas interaões a um protocolo, outras duas para pesquisa e listagem de protocolos.
Ao iniciar a aplicação é possivel consultar sua documentação disponivel no Gateway, bem como testa-la. Veja o capitulo para verificar os links uteis.

- POST: /api/interaction/start: Ira gerar um numero do protocolo.
- POST: /api/interaction/add: Ira adicionar novos eventos relacionados a um numero de protocolo. Essa API só pode ser executada para protocolos já existentes.
- GET: /api/interactions: Lista todos os protocolso registrados, é possivel filtrar essa buscar por todos os atributos de um protocolo.
- GET: /api/interacion/{protocolNumber}: Ira listar todos os eventos relacionados a um numero de protocolo.

Todas os endpoints são protegidos, para utiliza-los é necessário gerar um Token JWT, esse processo está descrito no capitulo [4.1 Gerando Token de Autorização].
Temos também abaixo exemplos de como utilizar as operaçoes:

- POST: /api/interaction/start
- POST: /api/interaction/add

Não esta contemplado exemplos de utilização das operações de busca.

#### 4.1 Gerando Token de Autorização

```unix
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "password": "admin", \ 
   "username": "admin" \ 
 }' 'http://localhost:8080/api/authenticate'
```

resposta:
```json
{
  "id_token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTUwOTcyNDc4OX0.Awst2iN56DZLJt-wWLGkzZSUN-YaiA1hVkanBirVrNX6pQbxy1NJeZcJ8IPoyB_GsX7_q_GMs1HHg47jxSHPew"
}
```

#### 4.2 Gerando um novo Protocolo

Request:

```json
{
  "applicationId": 0,
  "customerId": 0,
  "eventType": "ATTENDANCE"
}
```

Request de exemplo via curl:
```unix
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTUwOTcyNDc2OH0.pl3xS5cQVhf5P-7X2zDhyBIYrzWA91d6xUvTUasVbt5wYsocVlPpnBgJEVMF1PZXNd15AWQW39oR3xCcPENQOA' -d '{ \ 
   "applicationId": 12314, \ 
   "customerId": 123141, \ 
   "eventType": "ATTENDANCE" \ 
 }' 'http://localhost:8080/interaction/api/interaction/start'
```

Resposta: 
```json
{
  "id": 953,
  "protocolNumber": "20171102160601893",
  "applicationId": 12314,
  "customerId": 123141,
  "eventType": "ATTENDANCE",
  "updatedAt": "2017-11-02",
  "feedback": null
}
```

#### 4.3 Adicionando novas interações a um protocolo

Request:

```json
{
  "applicationId": 0,
  "customerId": 0,
  "eventType": "ATTENDANCE",
  "feedback": "string",
  "protocolNumber": "string"
}
```

Request de exemplo via curl:
```unix
curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTUwOTcyNDc2OH0.pl3xS5cQVhf5P-7X2zDhyBIYrzWA91d6xUvTUasVbt5wYsocVlPpnBgJEVMF1PZXNd15AWQW39oR3xCcPENQOA' -d '{ \ 
   "applicationId": 312321, \ 
   "customerId": 321312, \ 
   "eventType": "EVALUATION", \ 
   "feedback": "GOOD", \ 
   "protocolNumber": "20171102160601893" \ 
 }' 
```

Retorno:
```json
{
  "id": 954,
  "protocolNumber": "20171102160601893",
  "applicationId": 312321,
  "customerId": 321312,
  "eventType": "EVALUATION",
  "updatedAt": "2017-11-02",
  "feedback": "GOOD"
}
```

### 5. Outras informações

#### 5.1. Endpoints uteis

- gateway-console: http://localhost:8080
- swagger-docs: http://localhost:8080/#/docs
- registry: http://localhost:8761/

#### 5.2. Usuario Padrão

Todas as URL podem ser acessadas com o usuário e senha DEFAULT do Jhipster:

- user: admin
- pass: admin

