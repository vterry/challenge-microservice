package com.challenge.microservice.interaction.repository;

import com.challenge.microservice.interaction.domain.Interaction;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Interaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InteractionRepository extends JpaRepository<Interaction, Long>, JpaSpecificationExecutor<Interaction> {

    public List<Interaction> findByProtocolNumber(String protocolNumber);
}
