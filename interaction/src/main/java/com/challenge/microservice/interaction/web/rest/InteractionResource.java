package com.challenge.microservice.interaction.web.rest;

import com.challenge.microservice.interaction.service.wrapper.RequestProtocol;
import com.challenge.microservice.interaction.web.rest.util.NumberGenerator;
import com.codahale.metrics.annotation.Timed;
import com.challenge.microservice.interaction.service.InteractionService;
import com.challenge.microservice.interaction.web.rest.util.HeaderUtil;
import com.challenge.microservice.interaction.service.dto.InteractionDTO;
import com.challenge.microservice.interaction.service.dto.InteractionCriteria;
import com.challenge.microservice.interaction.service.InteractionQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Interaction.
 */
@RestController
@RequestMapping("/api")
public class InteractionResource {

    private final Logger log = LoggerFactory.getLogger(InteractionResource.class);

    private static final String ENTITY_NAME = "interaction";

    private final InteractionService interactionService;

    private final InteractionQueryService interactionQueryService;

    public InteractionResource(InteractionService interactionService, InteractionQueryService interactionQueryService) {
        this.interactionService = interactionService;
        this.interactionQueryService = interactionQueryService;
    }

    /**
     * POST  /interaction : It will create a Protocol that represents an Interaction between Customer and Company.
     *
     * @param requestProtocol the interactionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new interactionDTO, or with status 400 (Bad Request) if the interaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/interaction/start")
    @Timed
    public ResponseEntity<InteractionDTO> createInteraction(@Valid @RequestBody RequestProtocol requestProtocol) throws URISyntaxException {
        log.debug("REST request to save Interaction : {}", requestProtocol);

        InteractionDTO interactionDTO = new InteractionDTO();

        interactionDTO.setProtocolNumber(NumberGenerator.generate());
        interactionDTO.setApplicationId(requestProtocol.getApplicationId());
        interactionDTO.setCustomerId(requestProtocol.getCustomerId());
        interactionDTO.setEventType(requestProtocol.getEventType());
        interactionDTO.setUpdatedAt(LocalDate.now());

        InteractionDTO result = interactionService.save(interactionDTO);

        return ResponseEntity.created(new URI("/api/interaction/" + result.getProtocolNumber()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getProtocolNumber().toString()))
            .body(result);
    }

    /**
     * POST  /interaction : This will add into a Protocol different kinds of Interactions among these options:
     * - ATTENDANCE = Represents a common interaction.
     * - COMPLAINT = To register a complaint from a customer about the company.
     * - EVALUATION = To register evalution from customer over the services provides by Company.
     *
     * @param interactionDTO the interactionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new interactionDTO, or with status 400 (Bad Request) if the interaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/interaction/add")
    public ResponseEntity<InteractionDTO> addInteraction(@Valid @RequestBody InteractionDTO interactionDTO) throws Exception {

        if(interactionService.findByProtocolNumber(interactionDTO.getProtocolNumber()) == null){
            throw new URISyntaxException("Error to add an Interaction", "Protocol not found");
        }

        interactionDTO.setUpdatedAt(LocalDate.now());

        InteractionDTO result = interactionService.save(interactionDTO);

        return ResponseEntity.created(new URI("/api/interaction/" + result.getProtocolNumber()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getProtocolNumber().toString()))
            .body(result);
    }

    /**
     * GET  /interactions : get all the interactions.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of interactions in body
     */
    @GetMapping("/interactions")
    @Timed
    public ResponseEntity<List<InteractionDTO>> getAllInteractions(InteractionCriteria criteria) {
        log.debug("REST request to get Interactions by criteria: {}", criteria);
        List<InteractionDTO> entityList = interactionQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /interaction/:protocolNumber : get the "protocolNumber" interaction.
     *
     * @param protocolNumber the id of the interactionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the interactionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/interaction/{protocolNumber}")
    @Timed
    public ResponseEntity<List<InteractionDTO>> getInteraction(@PathVariable String protocolNumber) {
        log.debug("REST request to get Interaction : {}", protocolNumber);
        List<InteractionDTO> entityList = interactionService.findByProtocolNumber(protocolNumber);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(entityList));
    }

}
