package com.challenge.microservice.interaction.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.challenge.microservice.interaction.domain.enumeration.EventType;

/**
 * A Interaction.
 */
@Entity
@Table(name = "interaction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Interaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "protocol_number")
    private String protocolNumber;

    @NotNull
    @Column(name = "application_id", nullable = false)
    private Integer applicationId;

    @NotNull
    @Column(name = "customer_id", nullable = false)
    private Integer customerId;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "event_type", nullable = false)
    private EventType eventType;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "feedback")
    private String feedback;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProtocolNumber() {
        return protocolNumber;
    }

    public Interaction protocolNumber(String protocolNumber) {
        this.protocolNumber = protocolNumber;
        return this;
    }

    public void setProtocolNumber(String protocolNumber) {
        this.protocolNumber = protocolNumber;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public Interaction applicationId(Integer applicationId) {
        this.applicationId = applicationId;
        return this;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public Interaction customerId(Integer customerId) {
        this.customerId = customerId;
        return this;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public EventType getEventType() {
        return eventType;
    }

    public Interaction eventType(EventType eventType) {
        this.eventType = eventType;
        return this;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public Interaction updatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFeedback() {
        return feedback;
    }

    public Interaction feedback(String feedback) {
        this.feedback = feedback;
        return this;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Interaction interaction = (Interaction) o;
        if (interaction.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), interaction.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Interaction{" +
            "id=" + getId() +
            ", protocolNumber='" + getProtocolNumber() + "'" +
            ", applicationId='" + getApplicationId() + "'" +
            ", customerId='" + getCustomerId() + "'" +
            ", eventType='" + getEventType() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", feedback='" + getFeedback() + "'" +
            "}";
    }
}
