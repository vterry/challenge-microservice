package com.challenge.microservice.interaction.service;

import com.challenge.microservice.interaction.domain.Interaction;
import com.challenge.microservice.interaction.repository.InteractionRepository;
import com.challenge.microservice.interaction.service.dto.InteractionDTO;
import com.challenge.microservice.interaction.service.mapper.InteractionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Interaction.
 */
@Service
@Transactional
public class InteractionService {

    private final Logger log = LoggerFactory.getLogger(InteractionService.class);

    private final InteractionRepository interactionRepository;

    private final InteractionMapper interactionMapper;

    public InteractionService(InteractionRepository interactionRepository, InteractionMapper interactionMapper) {
        this.interactionRepository = interactionRepository;
        this.interactionMapper = interactionMapper;
    }

    /**
     * Save a interaction.
     *
     * @param interactionDTO the entity to save
     * @return the persisted entity
     */
    public InteractionDTO save(InteractionDTO interactionDTO) {
        log.debug("Request to save Interaction : {}", interactionDTO);
        Interaction interaction = interactionMapper.toEntity(interactionDTO);
        interaction = interactionRepository.save(interaction);
        return interactionMapper.toDto(interaction);
    }

    /**
     *  Get all the interactions.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<InteractionDTO> findAll() {
        log.debug("Request to get all Interactions");
        return interactionRepository.findAll().stream()
            .map(interactionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one interaction by id.
     *
     *  @param protocolNumber the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public List<InteractionDTO> findByProtocolNumber(String protocolNumber) {
        log.debug("Request to get Interaction : {}", protocolNumber);
        List<Interaction> interaction = interactionRepository.findByProtocolNumber(protocolNumber);
        return interactionMapper.toDto(interaction);
    }

    /**
     *  Delete the  interaction by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Interaction : {}", id);
        interactionRepository.delete(id);
    }
}
