package com.challenge.microservice.interaction.service.wrapper;

import com.challenge.microservice.interaction.domain.enumeration.EventType;

import javax.validation.constraints.NotNull;

public class RequestProtocol {

    @NotNull
    private Integer applicationId;

    @NotNull
    private Integer customerId;

    @NotNull
    private EventType eventType;

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }
}
