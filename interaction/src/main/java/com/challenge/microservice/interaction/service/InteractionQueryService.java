package com.challenge.microservice.interaction.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.challenge.microservice.interaction.domain.Interaction;
import com.challenge.microservice.interaction.domain.*; // for static metamodels
import com.challenge.microservice.interaction.repository.InteractionRepository;
import com.challenge.microservice.interaction.service.dto.InteractionCriteria;

import com.challenge.microservice.interaction.service.dto.InteractionDTO;
import com.challenge.microservice.interaction.service.mapper.InteractionMapper;
import com.challenge.microservice.interaction.domain.enumeration.EventType;

/**
 * Service for executing complex queries for Interaction entities in the database.
 * The main input is a {@link InteractionCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link InteractionDTO} or a {@link Page} of {%link InteractionDTO} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class InteractionQueryService extends QueryService<Interaction> {

    private final Logger log = LoggerFactory.getLogger(InteractionQueryService.class);


    private final InteractionRepository interactionRepository;

    private final InteractionMapper interactionMapper;

    public InteractionQueryService(InteractionRepository interactionRepository, InteractionMapper interactionMapper) {
        this.interactionRepository = interactionRepository;
        this.interactionMapper = interactionMapper;
    }

    /**
     * Return a {@link List} of {%link InteractionDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<InteractionDTO> findByCriteria(InteractionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Interaction> specification = createSpecification(criteria);
        return interactionMapper.toDto(interactionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {%link InteractionDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<InteractionDTO> findByCriteria(InteractionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Interaction> specification = createSpecification(criteria);
        final Page<Interaction> result = interactionRepository.findAll(specification, page);
        return result.map(interactionMapper::toDto);
    }

    /**
     * Function to convert InteractionCriteria to a {@link Specifications}
     */
    private Specifications<Interaction> createSpecification(InteractionCriteria criteria) {
        Specifications<Interaction> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Interaction_.id));
            }
            if (criteria.getProtocolNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProtocolNumber(), Interaction_.protocolNumber));
            }
            if (criteria.getApplicationId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getApplicationId(), Interaction_.applicationId));
            }
            if (criteria.getCustomerId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCustomerId(), Interaction_.customerId));
            }
            if (criteria.getEventType() != null) {
                specification = specification.and(buildSpecification(criteria.getEventType(), Interaction_.eventType));
            }
            if (criteria.getUpdatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedAt(), Interaction_.updatedAt));
            }
            if (criteria.getFeedback() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFeedback(), Interaction_.feedback));
            }
        }
        return specification;
    }

}
