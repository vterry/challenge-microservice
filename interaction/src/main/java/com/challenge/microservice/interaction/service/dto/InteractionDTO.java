package com.challenge.microservice.interaction.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.challenge.microservice.interaction.domain.enumeration.EventType;

/**
 * A DTO for the Interaction entity.
 */
public class InteractionDTO implements Serializable {

    private Long id;

    private String protocolNumber;

    @NotNull
    private Integer applicationId;

    @NotNull
    private Integer customerId;

    @NotNull
    private EventType eventType;

    private LocalDate updatedAt;

    private String feedback;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProtocolNumber() {
        return protocolNumber;
    }

    public void setProtocolNumber(String protocolNumber) {
        this.protocolNumber = protocolNumber;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InteractionDTO interactionDTO = (InteractionDTO) o;
        if(interactionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), interactionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InteractionDTO{" +
            "id=" + getId() +
            ", protocolNumber='" + getProtocolNumber() + "'" +
            ", applicationId='" + getApplicationId() + "'" +
            ", customerId='" + getCustomerId() + "'" +
            ", eventType='" + getEventType() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", feedback='" + getFeedback() + "'" +
            "}";
    }
}
