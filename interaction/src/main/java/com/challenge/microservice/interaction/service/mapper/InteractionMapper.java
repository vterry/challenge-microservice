package com.challenge.microservice.interaction.service.mapper;

import com.challenge.microservice.interaction.domain.*;
import com.challenge.microservice.interaction.service.dto.InteractionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Interaction and its DTO InteractionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InteractionMapper extends EntityMapper<InteractionDTO, Interaction> {

    default Interaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        Interaction interaction = new Interaction();
        interaction.setId(id);
        return interaction;
    }
}
