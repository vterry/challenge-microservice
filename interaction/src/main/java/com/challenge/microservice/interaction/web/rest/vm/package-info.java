/**
 * View Models used by Spring MVC REST controllers.
 */
package com.challenge.microservice.interaction.web.rest.vm;
