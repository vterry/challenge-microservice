package com.challenge.microservice.interaction.service.dto;

import java.io.Serializable;
import com.challenge.microservice.interaction.domain.enumeration.EventType;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;


import io.github.jhipster.service.filter.LocalDateFilter;



/**
 * Criteria class for the Interaction entity. This class is used in InteractionResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /interactions?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class InteractionCriteria implements Serializable {
    /**
     * Class for filtering EventType
     */
    public static class EventTypeFilter extends Filter<EventType> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter protocolNumber;

    private IntegerFilter applicationId;

    private IntegerFilter customerId;

    private EventTypeFilter eventType;

    private LocalDateFilter updatedAt;

    private StringFilter feedback;

    public InteractionCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getProtocolNumber() {
        return protocolNumber;
    }

    public void setProtocolNumber(StringFilter protocolNumber) {
        this.protocolNumber = protocolNumber;
    }

    public IntegerFilter getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(IntegerFilter applicationId) {
        this.applicationId = applicationId;
    }

    public IntegerFilter getCustomerId() {
        return customerId;
    }

    public void setCustomerId(IntegerFilter customerId) {
        this.customerId = customerId;
    }

    public EventTypeFilter getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeFilter eventType) {
        this.eventType = eventType;
    }

    public LocalDateFilter getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateFilter updatedAt) {
        this.updatedAt = updatedAt;
    }

    public StringFilter getFeedback() {
        return feedback;
    }

    public void setFeedback(StringFilter feedback) {
        this.feedback = feedback;
    }

    @Override
    public String toString() {
        return "InteractionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (protocolNumber != null ? "protocolNumber=" + protocolNumber + ", " : "") +
                (applicationId != null ? "applicationId=" + applicationId + ", " : "") +
                (customerId != null ? "customerId=" + customerId + ", " : "") +
                (eventType != null ? "eventType=" + eventType + ", " : "") +
                (updatedAt != null ? "updatedAt=" + updatedAt + ", " : "") +
                (feedback != null ? "feedback=" + feedback + ", " : "") +
            "}";
    }

}
