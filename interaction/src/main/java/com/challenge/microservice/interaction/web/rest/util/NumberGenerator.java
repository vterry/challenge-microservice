package com.challenge.microservice.interaction.web.rest.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NumberGenerator {

    public static String generate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSS");
        Date date = new Date();
        return dateFormat.format(date).toString();
    }
}
