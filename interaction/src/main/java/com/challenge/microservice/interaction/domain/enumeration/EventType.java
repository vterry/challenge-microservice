package com.challenge.microservice.interaction.domain.enumeration;

/**
 * The EventType enumeration.
 */
public enum EventType {
    ATTENDANCE,  COMPLAINT,  EVALUATION
}
