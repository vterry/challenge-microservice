package com.challenge.microservice.interaction.web.rest;

import com.challenge.microservice.interaction.InteractionApp;

import com.challenge.microservice.interaction.domain.Interaction;
import com.challenge.microservice.interaction.repository.InteractionRepository;
import com.challenge.microservice.interaction.service.InteractionService;
import com.challenge.microservice.interaction.service.dto.InteractionDTO;
import com.challenge.microservice.interaction.service.mapper.InteractionMapper;
import com.challenge.microservice.interaction.service.wrapper.RequestProtocol;
import com.challenge.microservice.interaction.web.rest.errors.ExceptionTranslator;
import com.challenge.microservice.interaction.service.dto.InteractionCriteria;
import com.challenge.microservice.interaction.service.InteractionQueryService;

import com.challenge.microservice.interaction.web.rest.util.NumberGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.challenge.microservice.interaction.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.challenge.microservice.interaction.domain.enumeration.EventType;
/**
 * Test class for the InteractionResource REST controller.
 *
 * @see InteractionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = InteractionApp.class)
public class InteractionResourceIntTest {

    private static final String DEFAULT_PROTOCOL_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PROTOCOL_NUMBER = "BBBBBBBBBB";

    private static final Integer DEFAULT_APPLICATION_ID = 1;
    private static final Integer UPDATED_APPLICATION_ID = 2;

    private static final Integer DEFAULT_CUSTOMER_ID = 1;
    private static final Integer UPDATED_CUSTOMER_ID = 2;

    private static final EventType DEFAULT_EVENT_TYPE = EventType.ATTENDANCE;
    private static final EventType UPDATED_EVENT_TYPE = EventType.COMPLAINT;

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_FEEDBACK = "AAAAAAAAAA";
    private static final String UPDATED_FEEDBACK = "BBBBBBBBBB";

    @Autowired
    private InteractionRepository interactionRepository;

    @Autowired
    private InteractionMapper interactionMapper;

    @Autowired
    private InteractionService interactionService;

    @Autowired
    private InteractionQueryService interactionQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInteractionMockMvc;

    private Interaction interaction;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InteractionResource interactionResource = new InteractionResource(interactionService, interactionQueryService);
        this.restInteractionMockMvc = MockMvcBuilders.standaloneSetup(interactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Interaction createEntity(EntityManager em) {
        Interaction interaction = new Interaction()
            .protocolNumber(DEFAULT_PROTOCOL_NUMBER)
            .applicationId(DEFAULT_APPLICATION_ID)
            .customerId(DEFAULT_CUSTOMER_ID)
            .eventType(DEFAULT_EVENT_TYPE)
            .updatedAt(DEFAULT_UPDATED_AT)
            .feedback(DEFAULT_FEEDBACK);
        return interaction;
    }

    @Before
    public void initTest() {
        interaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createInteraction() throws Exception {
        int databaseSizeBeforeCreate = interactionRepository.findAll().size();

        // Create the Interaction
        RequestProtocol requestProtocol = new RequestProtocol();
        requestProtocol.setApplicationId(DEFAULT_APPLICATION_ID);
        requestProtocol.setCustomerId(DEFAULT_CUSTOMER_ID);
        requestProtocol.setEventType(DEFAULT_EVENT_TYPE);


        restInteractionMockMvc.perform(post("/api/interaction")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(requestProtocol)))
            .andExpect(status().isCreated());

        // Validate the Interaction in the database
        List<Interaction> interactionList = interactionRepository.findAll();
        assertThat(interactionList).hasSize(databaseSizeBeforeCreate + 1);
        Interaction testInteraction = interactionList.get(interactionList.size() - 1);
        assertThat(testInteraction.getApplicationId()).isEqualTo(DEFAULT_APPLICATION_ID);
        assertThat(testInteraction.getCustomerId()).isEqualTo(DEFAULT_CUSTOMER_ID);
        assertThat(testInteraction.getEventType()).isEqualTo(DEFAULT_EVENT_TYPE);
    }


    @Test
    @Transactional
    public void checkCustomerIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = interactionRepository.findAll().size();
        // set the field null
        interaction.setCustomerId(null);

        // Create the Interaction, which fails.
        InteractionDTO interactionDTO = interactionMapper.toDto(interaction);

        restInteractionMockMvc.perform(post("/api/interaction")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(interactionDTO)))
            .andExpect(status().isBadRequest());

        List<Interaction> interactionList = interactionRepository.findAll();
        assertThat(interactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEventTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = interactionRepository.findAll().size();
        // set the field null
        interaction.setEventType(null);

        // Create the Interaction, which fails.
        InteractionDTO interactionDTO = interactionMapper.toDto(interaction);

        restInteractionMockMvc.perform(post("/api/interaction")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(interactionDTO)))
            .andExpect(status().isBadRequest());

        List<Interaction> interactionList = interactionRepository.findAll();
        assertThat(interactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInteractions() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList
        restInteractionMockMvc.perform(get("/api/interactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(interaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].protocolNumber").value(hasItem(DEFAULT_PROTOCOL_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].applicationId").value(hasItem(DEFAULT_APPLICATION_ID)))
            .andExpect(jsonPath("$.[*].customerId").value(hasItem(DEFAULT_CUSTOMER_ID)))
            .andExpect(jsonPath("$.[*].eventType").value(hasItem(DEFAULT_EVENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
            .andExpect(jsonPath("$.[*].feedback").value(hasItem(DEFAULT_FEEDBACK.toString())));
    }

    @Test
    @Transactional
    public void getInteraction() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get the interaction
        restInteractionMockMvc.perform(get("/api/interaction/{protocolNumber}", interaction.getProtocolNumber()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(interaction.getId().intValue()))
            .andExpect(jsonPath("$.protocolNumber").value(DEFAULT_PROTOCOL_NUMBER.toString()))
            .andExpect(jsonPath("$.applicationId").value(DEFAULT_APPLICATION_ID))
            .andExpect(jsonPath("$.customerId").value(DEFAULT_CUSTOMER_ID))
            .andExpect(jsonPath("$.eventType").value(DEFAULT_EVENT_TYPE.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.feedback").value(DEFAULT_FEEDBACK.toString()));
    }

    @Test
    @Transactional
    public void getAllInteractionsByProtocolNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where protocolNumber equals to DEFAULT_PROTOCOL_NUMBER
        defaultInteractionShouldBeFound("protocolNumber.equals=" + DEFAULT_PROTOCOL_NUMBER);

        // Get all the interactionList where protocolNumber equals to UPDATED_PROTOCOL_NUMBER
        defaultInteractionShouldNotBeFound("protocolNumber.equals=" + UPDATED_PROTOCOL_NUMBER);
    }

    @Test
    @Transactional
    public void getAllInteractionsByProtocolNumberIsInShouldWork() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where protocolNumber in DEFAULT_PROTOCOL_NUMBER or UPDATED_PROTOCOL_NUMBER
        defaultInteractionShouldBeFound("protocolNumber.in=" + DEFAULT_PROTOCOL_NUMBER + "," + UPDATED_PROTOCOL_NUMBER);

        // Get all the interactionList where protocolNumber equals to UPDATED_PROTOCOL_NUMBER
        defaultInteractionShouldNotBeFound("protocolNumber.in=" + UPDATED_PROTOCOL_NUMBER);
    }

    @Test
    @Transactional
    public void getAllInteractionsByProtocolNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where protocolNumber is not null
        defaultInteractionShouldBeFound("protocolNumber.specified=true");

        // Get all the interactionList where protocolNumber is null
        defaultInteractionShouldNotBeFound("protocolNumber.specified=false");
    }

    @Test
    @Transactional
    public void getAllInteractionsByApplicationIdIsEqualToSomething() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where applicationId equals to DEFAULT_APPLICATION_ID
        defaultInteractionShouldBeFound("applicationId.equals=" + DEFAULT_APPLICATION_ID);

        // Get all the interactionList where applicationId equals to UPDATED_APPLICATION_ID
        defaultInteractionShouldNotBeFound("applicationId.equals=" + UPDATED_APPLICATION_ID);
    }

    @Test
    @Transactional
    public void getAllInteractionsByApplicationIdIsInShouldWork() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where applicationId in DEFAULT_APPLICATION_ID or UPDATED_APPLICATION_ID
        defaultInteractionShouldBeFound("applicationId.in=" + DEFAULT_APPLICATION_ID + "," + UPDATED_APPLICATION_ID);

        // Get all the interactionList where applicationId equals to UPDATED_APPLICATION_ID
        defaultInteractionShouldNotBeFound("applicationId.in=" + UPDATED_APPLICATION_ID);
    }

    @Test
    @Transactional
    public void getAllInteractionsByApplicationIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where applicationId is not null
        defaultInteractionShouldBeFound("applicationId.specified=true");

        // Get all the interactionList where applicationId is null
        defaultInteractionShouldNotBeFound("applicationId.specified=false");
    }

    @Test
    @Transactional
    public void getAllInteractionsByApplicationIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where applicationId greater than or equals to DEFAULT_APPLICATION_ID
        defaultInteractionShouldBeFound("applicationId.greaterOrEqualThan=" + DEFAULT_APPLICATION_ID);

        // Get all the interactionList where applicationId greater than or equals to UPDATED_APPLICATION_ID
        defaultInteractionShouldNotBeFound("applicationId.greaterOrEqualThan=" + UPDATED_APPLICATION_ID);
    }

    @Test
    @Transactional
    public void getAllInteractionsByApplicationIdIsLessThanSomething() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where applicationId less than or equals to DEFAULT_APPLICATION_ID
        defaultInteractionShouldNotBeFound("applicationId.lessThan=" + DEFAULT_APPLICATION_ID);

        // Get all the interactionList where applicationId less than or equals to UPDATED_APPLICATION_ID
        defaultInteractionShouldBeFound("applicationId.lessThan=" + UPDATED_APPLICATION_ID);
    }


    @Test
    @Transactional
    public void getAllInteractionsByCustomerIdIsEqualToSomething() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where customerId equals to DEFAULT_CUSTOMER_ID
        defaultInteractionShouldBeFound("customerId.equals=" + DEFAULT_CUSTOMER_ID);

        // Get all the interactionList where customerId equals to UPDATED_CUSTOMER_ID
        defaultInteractionShouldNotBeFound("customerId.equals=" + UPDATED_CUSTOMER_ID);
    }

    @Test
    @Transactional
    public void getAllInteractionsByCustomerIdIsInShouldWork() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where customerId in DEFAULT_CUSTOMER_ID or UPDATED_CUSTOMER_ID
        defaultInteractionShouldBeFound("customerId.in=" + DEFAULT_CUSTOMER_ID + "," + UPDATED_CUSTOMER_ID);

        // Get all the interactionList where customerId equals to UPDATED_CUSTOMER_ID
        defaultInteractionShouldNotBeFound("customerId.in=" + UPDATED_CUSTOMER_ID);
    }

    @Test
    @Transactional
    public void getAllInteractionsByCustomerIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where customerId is not null
        defaultInteractionShouldBeFound("customerId.specified=true");

        // Get all the interactionList where customerId is null
        defaultInteractionShouldNotBeFound("customerId.specified=false");
    }

    @Test
    @Transactional
    public void getAllInteractionsByCustomerIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where customerId greater than or equals to DEFAULT_CUSTOMER_ID
        defaultInteractionShouldBeFound("customerId.greaterOrEqualThan=" + DEFAULT_CUSTOMER_ID);

        // Get all the interactionList where customerId greater than or equals to UPDATED_CUSTOMER_ID
        defaultInteractionShouldNotBeFound("customerId.greaterOrEqualThan=" + UPDATED_CUSTOMER_ID);
    }

    @Test
    @Transactional
    public void getAllInteractionsByCustomerIdIsLessThanSomething() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where customerId less than or equals to DEFAULT_CUSTOMER_ID
        defaultInteractionShouldNotBeFound("customerId.lessThan=" + DEFAULT_CUSTOMER_ID);

        // Get all the interactionList where customerId less than or equals to UPDATED_CUSTOMER_ID
        defaultInteractionShouldBeFound("customerId.lessThan=" + UPDATED_CUSTOMER_ID);
    }


    @Test
    @Transactional
    public void getAllInteractionsByEventTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where eventType equals to DEFAULT_EVENT_TYPE
        defaultInteractionShouldBeFound("eventType.equals=" + DEFAULT_EVENT_TYPE);

        // Get all the interactionList where eventType equals to UPDATED_EVENT_TYPE
        defaultInteractionShouldNotBeFound("eventType.equals=" + UPDATED_EVENT_TYPE);
    }

    @Test
    @Transactional
    public void getAllInteractionsByEventTypeIsInShouldWork() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where eventType in DEFAULT_EVENT_TYPE or UPDATED_EVENT_TYPE
        defaultInteractionShouldBeFound("eventType.in=" + DEFAULT_EVENT_TYPE + "," + UPDATED_EVENT_TYPE);

        // Get all the interactionList where eventType equals to UPDATED_EVENT_TYPE
        defaultInteractionShouldNotBeFound("eventType.in=" + UPDATED_EVENT_TYPE);
    }

    @Test
    @Transactional
    public void getAllInteractionsByEventTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where eventType is not null
        defaultInteractionShouldBeFound("eventType.specified=true");

        // Get all the interactionList where eventType is null
        defaultInteractionShouldNotBeFound("eventType.specified=false");
    }

    @Test
    @Transactional
    public void getAllInteractionsByUpdatedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where updatedAt equals to DEFAULT_UPDATED_AT
        defaultInteractionShouldBeFound("updatedAt.equals=" + DEFAULT_UPDATED_AT);

        // Get all the interactionList where updatedAt equals to UPDATED_UPDATED_AT
        defaultInteractionShouldNotBeFound("updatedAt.equals=" + UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    public void getAllInteractionsByUpdatedAtIsInShouldWork() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where updatedAt in DEFAULT_UPDATED_AT or UPDATED_UPDATED_AT
        defaultInteractionShouldBeFound("updatedAt.in=" + DEFAULT_UPDATED_AT + "," + UPDATED_UPDATED_AT);

        // Get all the interactionList where updatedAt equals to UPDATED_UPDATED_AT
        defaultInteractionShouldNotBeFound("updatedAt.in=" + UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    public void getAllInteractionsByUpdatedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where updatedAt is not null
        defaultInteractionShouldBeFound("updatedAt.specified=true");

        // Get all the interactionList where updatedAt is null
        defaultInteractionShouldNotBeFound("updatedAt.specified=false");
    }

    @Test
    @Transactional
    public void getAllInteractionsByUpdatedAtIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where updatedAt greater than or equals to DEFAULT_UPDATED_AT
        defaultInteractionShouldBeFound("updatedAt.greaterOrEqualThan=" + DEFAULT_UPDATED_AT);

        // Get all the interactionList where updatedAt greater than or equals to UPDATED_UPDATED_AT
        defaultInteractionShouldNotBeFound("updatedAt.greaterOrEqualThan=" + UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    public void getAllInteractionsByUpdatedAtIsLessThanSomething() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where updatedAt less than or equals to DEFAULT_UPDATED_AT
        defaultInteractionShouldNotBeFound("updatedAt.lessThan=" + DEFAULT_UPDATED_AT);

        // Get all the interactionList where updatedAt less than or equals to UPDATED_UPDATED_AT
        defaultInteractionShouldBeFound("updatedAt.lessThan=" + UPDATED_UPDATED_AT);
    }


    @Test
    @Transactional
    public void getAllInteractionsByFeedbackIsEqualToSomething() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where feedback equals to DEFAULT_FEEDBACK
        defaultInteractionShouldBeFound("feedback.equals=" + DEFAULT_FEEDBACK);

        // Get all the interactionList where feedback equals to UPDATED_FEEDBACK
        defaultInteractionShouldNotBeFound("feedback.equals=" + UPDATED_FEEDBACK);
    }

    @Test
    @Transactional
    public void getAllInteractionsByFeedbackIsInShouldWork() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where feedback in DEFAULT_FEEDBACK or UPDATED_FEEDBACK
        defaultInteractionShouldBeFound("feedback.in=" + DEFAULT_FEEDBACK + "," + UPDATED_FEEDBACK);

        // Get all the interactionList where feedback equals to UPDATED_FEEDBACK
        defaultInteractionShouldNotBeFound("feedback.in=" + UPDATED_FEEDBACK);
    }

    @Test
    @Transactional
    public void getAllInteractionsByFeedbackIsNullOrNotNull() throws Exception {
        // Initialize the database
        interactionRepository.saveAndFlush(interaction);

        // Get all the interactionList where feedback is not null
        defaultInteractionShouldBeFound("feedback.specified=true");

        // Get all the interactionList where feedback is null
        defaultInteractionShouldNotBeFound("feedback.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultInteractionShouldBeFound(String filter) throws Exception {
        restInteractionMockMvc.perform(get("/api/interactions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(interaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].protocolNumber").value(hasItem(DEFAULT_PROTOCOL_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].applicationId").value(hasItem(DEFAULT_APPLICATION_ID)))
            .andExpect(jsonPath("$.[*].customerId").value(hasItem(DEFAULT_CUSTOMER_ID)))
            .andExpect(jsonPath("$.[*].eventType").value(hasItem(DEFAULT_EVENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
            .andExpect(jsonPath("$.[*].feedback").value(hasItem(DEFAULT_FEEDBACK.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultInteractionShouldNotBeFound(String filter) throws Exception {
        restInteractionMockMvc.perform(get("/api/interactions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingInteraction() throws Exception {
        // Get the interaction
        restInteractionMockMvc.perform(get("/api/interaction/{protocolNumber}", NumberGenerator.generate()))
            .andExpect(status().isNotFound());
    }


    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Interaction.class);
        Interaction interaction1 = new Interaction();
        interaction1.setId(1L);
        Interaction interaction2 = new Interaction();
        interaction2.setId(interaction1.getId());
        assertThat(interaction1).isEqualTo(interaction2);
        interaction2.setId(2L);
        assertThat(interaction1).isNotEqualTo(interaction2);
        interaction1.setId(null);
        assertThat(interaction1).isNotEqualTo(interaction2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(InteractionDTO.class);
        InteractionDTO interactionDTO1 = new InteractionDTO();
        interactionDTO1.setId(1L);
        InteractionDTO interactionDTO2 = new InteractionDTO();
        assertThat(interactionDTO1).isNotEqualTo(interactionDTO2);
        interactionDTO2.setId(interactionDTO1.getId());
        assertThat(interactionDTO1).isEqualTo(interactionDTO2);
        interactionDTO2.setId(2L);
        assertThat(interactionDTO1).isNotEqualTo(interactionDTO2);
        interactionDTO1.setId(null);
        assertThat(interactionDTO1).isNotEqualTo(interactionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(interactionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(interactionMapper.fromId(null)).isNull();
    }
}
